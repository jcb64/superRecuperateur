package main.java.gestionWeb;

import main.java.gestionWeb.siteReader.ScanVf;
import main.java.gestionWeb.siteReader.SiteReader;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.SQLOutput;
import java.util.List;

public class Download {
    private final static String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0";

    private String url;

    /**
     * On renseigne forcément une url à l'appelle du download
     *
     * @param url
     */
    public Download(String url) {
        this.url = url;
    }


    public List<String> listeManga() {
        SiteReader reader = null;
        System.out.println(url);

        if (url.contains("www.scan-vf.co")) {
            reader = new ScanVf();
        }
        downloadPage(reader);
        return reader.getListeManga();
    }

    private void downloadPage(SiteReader reader) {

        System.out.println(System.getProperties().toString());
        System.out.println("GET Request Using HttpURLConnection");
        try {
            String username="hitenpratap";
            StringBuilder stringBuilder = new StringBuilder("https://www.scan-vf.co/manga-list");
            stringBuilder.append("?q=");
            stringBuilder.append(URLEncoder.encode(username, "UTF-8"));

            URL obj = new URL(stringBuilder.toString());

            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Charset", "UTF-8");

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String line;
            StringBuffer response = new StringBuffer();

            while ((line = in.readLine()) != null) {
                response.append(line);
            }
            in.close();

            System.out.println(response.toString());
            reader.analysOfPage(response.toString());
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
}
