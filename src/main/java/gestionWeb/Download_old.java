package main.java.gestionWeb;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;

public class Download_old {

    private String url;

    public Download_old(String url) {
        this.url = url;
    }

    /**
     * Permet de verifier l'url envoye
     * If check == 0 site unknown
     */
    public int checkUrl() {

        int check;
        String site;
        System.out.println(url);
        if (this.url.contains("www.youtube.com")) {
            System.out.println("L'url vient de youtube");
            check = 1;
        } else if (this.url.contains("ww2.scan-vf.com")) {
            System.out.println("L'url vient de scan-vf");
            check = 2;
        } else {
            System.out.println("L'url est inconnu");
            check = 0;
        }

        return check;
    }
    /**
     * Compatible avec le site http://ww2.scan-vf.com/
     * Permet de récupérer toutes les pages d'un chapitre
     * @param chapitre chapitre à récupérer
     * @param page
     */
    public void getManga(String parametre) {

        Boolean continuer = true;
        String pageStr;
        String urlTemp;
        String name;
        Download_old dowTemp;
        Document doc = new Document();

        int chapitre;
        int page;

        String substitueChapitre;
        String substituePage;

        chapitre = 1;
        page = 1;
        substitueChapitre = "&&";
        substituePage = "@@";
        name = "manga";

        int index;
        String memo;
        int essai;

        System.out.println("les parametres = " + parametre);
        if (!parametre.equals("")) {
            if(parametre.contains("-subChap:")) {
                index = parametre.indexOf("-subChap:");
                index = index + "-subChap:".length();
                memo = parametre.substring(index, parametre.length());
                index = memo.indexOf(" ");
                if (index < 0) {
                    index = memo.length();
                }
                substitueChapitre = memo.substring(0, index);
            }

            if (parametre.contains("-subPage:")) {
                index = parametre.indexOf("-subPage:");
                index = index + "-subPage:".length();
                memo = parametre.substring(index, parametre.length());
                index = memo.indexOf(" ");
                if (index < 0) {
                    index = memo.length();
                }
                substituePage = memo.substring(0, index);
            }

            if (parametre.contains("-chap:")) {
                index = parametre.indexOf("-chap:");
                index = index + "-chap:".length();
                memo = parametre.substring(index, parametre.length());
                index = memo.indexOf(" ");
                if (index < 0) {
                    index = memo.length();
                }
                chapitre = Integer.parseInt(memo.substring(0, index));
            }

            if (parametre.contains("-page:")) {
                index = parametre.indexOf("-page:");
                index = index + "-page:".length();
                memo = parametre.substring(index, parametre.length());
                index = memo.indexOf(" ");
                if (index < 0) {
                    index = memo.length();
                }
                page = Integer.parseInt(memo.substring(0, index));
            }
            if (parametre.contains("-name:")) {
                index = parametre.indexOf("-name:");
                index = index + "-name:".length();
                memo = parametre.substring(index, parametre.length());
                index = memo.indexOf(" ");
                if (index < 0) {
                    index = memo.length();
                }
                name = memo.substring(0, index);
            }


        }

        int memoChapitre = chapitre;
        essai = 2;
        while(continuer) {
            urlTemp = this.url;
            urlTemp = urlTemp.replaceFirst(substitueChapitre,  Integer.toString(chapitre));
            //System.out.println(urlTemp);
            essai = 2;
            pageStr = Integer.toString(page);
            if (page < 10) {
                pageStr = 0 + pageStr;
            }
            urlTemp = urlTemp.replaceFirst(substituePage, pageStr);
            page++;
            System.out.println(urlTemp);
            dowTemp = new Download_old(urlTemp);

            if (!dowTemp.getFile(0)) {
                System.out.println(" -- essai " +essai + memoChapitre);
                essai--;
                if (chapitre == memoChapitre) {
                    doc.saveFile(name + "-" + chapitre);
                    chapitre++;
                    page = 1;
                }
                if (essai == 0) {
                    return;
                }
            } else {

                memoChapitre = chapitre;
                essai = 2;
            }
        }

    }
    /**
     * Permet de récupérer les images d'un site
     */
    public Boolean getFile(int essai) {
        InputStream input = null;
        FileOutputStream writeFile = null;
        String fileName;
        Boolean result;

        result = false;

        try {
            URL url = new URL(this.url);
            URLConnection connection = url.openConnection();
            connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.183 Safari/537.36 Vivaldi/1.96.1147.42");

            int fileLength = connection.getContentLength();

            if (fileLength == -1) {
                System.out.println("Mauvaise URL : essaie avec une autre extension.");
                if (this.url.contains(".png")) {
                    this.setUrl(this.url.replaceFirst(".png", ".jpg"));
                } else if (this.url.contains(".jpg")) {
                    this.setUrl(this.url.replaceFirst(".jpg", ".jpeg"));
                } else {
                    this.setUrl(this.url.replaceFirst(".jpeg", ".png"));

                }

                if (essai < 2) {
                    essai++;
                    result = this.getFile(essai);
                }
                return result;
            }

            input = connection.getInputStream();
            fileName = url.getFile().substring(url.getFile().lastIndexOf('/') + 1);
            fileName = "file/".concat(fileName);
            writeFile = new FileOutputStream(fileName);
            byte[] buffer = new byte[1024];
            int read;

            while ((read = input.read(buffer)) > 0) {
                writeFile.write(buffer, 0, read);
            }
            writeFile.flush();
            result = true;
        } catch (IOException e) {
            System.out.println("Error while trying to download the file.");
            e.printStackTrace();
        } catch(NullPointerException e) {
           e.printStackTrace();

        } finally {
            try {
                writeFile.close();
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                return result;
            }
        }
        return result;
    }

    public void parcoursPage() {

        String s;
        int inBody = 0;
        int index;
        Boolean result;
        try {
            BufferedReader r = new BufferedReader(new InputStreamReader(new URL(url).openStream()));


            Download_old download;
            while ((s = r.readLine()) != null) {
                if (inBody == 1) {
                    if(s.contains("</body>")) {
                        inBody = 0;
                        return ;
                    }
                    if (s.contains("<img")) {
                        index = s.indexOf("src=");
                        s = s.substring(index + 5);
                        index = s.indexOf("\"");
                        s = s.substring(0,index);
                        if (s.contains("http") && s.indexOf("http") == 0) {
                            download = new Download_old(s);
                            download.getFile(0);
                        }
                    }

                }
                if(s.contains("<body")) {
                    inBody = 1;
                }

            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }



}
