package main.java.gestionWeb.siteReader;

import java.util.List;

public interface SiteReader {


    List<String> getListeManga();
    void analysOfPage(String s);

    void resetFlag();

}
