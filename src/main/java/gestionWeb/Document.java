package main.java.gestionWeb;

import java.io.*;
import java.util.ArrayList;

public class Document {

    private File racine;

    public Document(){
        this.racine = new File("file");
    }
    public Document(String racine){
        this.racine = new File(racine);
    }

    private ArrayList<File> listFile() {
        ArrayList<File> files = new ArrayList<>();
        File dossier = new File("file/");
        String [] listefichiers;
        int i;

        listefichiers = dossier.list();
        for(i = 0; i < listefichiers.length; i++){
            files.add(new File(listefichiers[i]));
        }
        return files;
    }
    public boolean copier(File source, File dest) {
        try (InputStream sourceFile = new java.io.FileInputStream(source);
             OutputStream destinationFile = new FileOutputStream(dest)) {
            // Lecture par segment de 0.5Mo
            byte buffer[] = new byte[512 * 1024];
            int nbLecture;
            while ((nbLecture = sourceFile.read(buffer)) != -1){
                destinationFile.write(buffer, 0, nbLecture);
            }
        } catch (IOException e){
            e.printStackTrace();
            return false; // Erreur
        }
        return true; // Résultat OK
    }

    /**
     * Supprime le dossier File temporaire.
     */
     private void deleteDirectory() {
        if( racine.exists() ) {
            File[] files = racine.listFiles();
            for(File e : files) {
                e.delete();
            }
        }
    }


    public void saveFile(String racine) {
        File saveDoc;
        File file = new File("file");
        ArrayList<File> files = listFile();

        saveDoc = new File("saveDoc/"+racine);
        saveDoc.mkdirs();
        for(File e: files) {

            try (InputStream sourceFile = new java.io.FileInputStream("file/"+e.getName());
                 OutputStream destinationFile = new FileOutputStream(saveDoc.getPath() + "/" + e.getName())) {
                // Lecture par segment de 0.5Mo
                byte buffer[] = new byte[512 * 1024];
                int nbLecture;
                while ((nbLecture = sourceFile.read(buffer)) != -1){
                   destinationFile.write(buffer, 0, nbLecture);
               }
            } catch (IOException ex){
                ex.printStackTrace();
            }

        }
        deleteDirectory();
    }

}
